[TOC]

# Rest API Access

### Usage and Authorization
Authorize your API requests by generating a token, generate a token by creating a new user account.

By default middleware tries to find the token from `Authorization` header. You can authorize each requests by setting
your `Authorization` header value.

`authorization` header = `Bearer (token value)`


## Authentication

### Create Account

`POST Request`

```
Endpoint   /api/v1/auth/create
```

```
Payload
{
	"firstname": "John",
	"lastname": "Doe",
	"country": "+233",
	"account": "some@emailaddress.com",
	"password": "@test*123",
	"image": ""
}
```

> Noteworthy: _account can be either email or phone number eg: +23350100000_ and country requires the dialing code for each country eg +233 for Ghana, +234 for Nigeria and +27 for South Africa, Country should be saved for the first time in-app to prevent selecting country on succeeding logins

Success Response

```
{
    "status": "success",
    "response": {
        "firstname": "John",
        "lastname": "Doe",
        "country": "+233",
        "account": "some@emailaddress.com",
        "image": "default",
        "id": "46",
        "token": "eyJ0eXAiOiJKV1QiLvaJFCvG66lyIalJIb1jU...",
        "expiration": "1561221163"
    },
    "code": "200"
}
```

Account Exist

```
{
    "status": "error",
    "message": "Account exist",
    "code": "401"
}
```

Invalid Payload

```
{
    "status": "error",
    "message": "Bad Request",
    "code": "401"
}
```

### Sign In

`POST Request`

```
Endpoint   /api/v1/auth/signin
```

```
Payload
{
	"country": "+233",
	"account": "some@emailaddress.com",
	"password": "@test*123"
}
```

> Noteworthy: _account can be either email or phone number eg: +23350100000_ and country should not be asked on login but user should be able to change country

Success Response

```
{
    "status": "success",
    "response": {
        "id": "46",
        "firstname": "John",
        "lastname": "Doe",
        "country": "+233",
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9....",
        "expiration": "1561221163",
        "account": "some@emailaddress.com",
        "worker": "1",
        "image": "default",
        "updated": "2019-06-07 16:32:43.000000",
        "created": "2019-06-07 16:32:43.000000"
    },
    "code": "200"
}
```

Invalid Credentials

```
{
    "status": "error",
    "message": "Sign in error, Invalid credentials",
    "code": "401"
}
```

Invalid Payload

```
{
    "status": "error",
    "message": "Bad Request",
    "code": "401"
}
```

### Account Recovery

`PUT Request`

```
Endpoint   /api/v1/auth/recovery
```

```
Payload
{
	"account": "some@emailaddress.com"
}
```

> Noteworthy: _account value can be either email or phone number eg: +23350100000_

Success Response

```
{
    "status": "success",
    "message": "Password Updated",
    "code": "200"
}
```

Invalid Credentials

```
{
    "status": "error",
    "message": "Recovery error, Invalid credentials",
    "code": "401"
}
```

Invalid Payload

```
{
    "status": "error",
    "message": "Bad Request",
    "code": "401"
}
```


### Search

`GET Request`

```
Endpoint   /api/v1/search/services/[{term}]
```

> Noteworthy: `[{term}]` accepts `String` either title or type of service eg: Photography or Beauty and Spa

Sample Response

```
{
    "status": "success",
    "response": [
        {
            "id": "1039",
            "title": "Baby Photoshoot",
            "keywords": "Baby Shower, Infant, Child, Photographer, Photography",
            "worker": "Baby Photographers",
            "price": "GHS 305 to GHS 800",
            "type": "Photography",
            "image": "/images/book/baby.jpg"
        },
        {
            "id": "1040",
            "title": "Pre Wedding Photoshoot",
            "keywords": "Weddings, Photographers",
            "worker": "Pre Wedding Photographers",
            "price": "GHS 1,232 to GHS 2,100",
            "type": "Photography",
            "image": "/images/book/pre-wedding.jpg"
        }
    ],
    "code": "200"
}
```

### Services
#### All Services 

`GET Request`

```
Endpoint   /api/v1/services/all
```
Sample Response

```
{
    "status": "success",
    "response": [
        {
            "id": "1001",
            "title": "Phone and Tablet Repair Services",
            "keywords": "Mobile Repairs, Repair Iphone, Repair Ipad, Repair Samsung, Fix Phone",
            "worker": "Mobile Technicians",
            "price": "GHS 30 to GHS 286",
            "type": "Repair Services",
            "image": "/images/book/phone.jpg"
        },
        {
            "id": "1002",
            "title": "Computer Repair and Laptop Services",
            "keywords": "Repair Macbook, Repair Laptop, Repair Dell, Repair Acer, Repair Alienware, Repair Samsung, Repair Hp, Repair Toshiba, Fix laptop, Fix pc, Fix computer, Computer Repair",
            "worker": "Computer Technicians",
            "price": "GHS 50 to GHS 634",
            "type": "Repair Services",
            "image": "/images/book/computer.jpg"
        },
        {
            "id": "1003",
            "title": "Air condition service and Repair",
            "keywords": "AC Service Repair",
            "worker": "Air Condition Repairers",
            "price": "GHS 42 to GHS 128",
            "type": "Repair Services",
            "image": "/images/book/air-condition.jpg"
        },
        {
            "id": "1004",
            "title": "Refrigerator Service and Repair",
            "keywords": "fridge repairers, fix freezers",
            "worker": "Refrigerator Repairers",
            "price": "GHS 15 to GHS 34",
            "type": "Repair Services",
            "image": "/images/book/refrigerator.jpg"
        },
        {
            "id": "1005",
            "title": "Washing Machine Repair Services",
            "keywords": "front-load washing machine, top-load washing machine, twin tup washing machine, fix washing machine",
            "worker": "Washing Machine Repairers",
            "price": "GHS 15 to GHS 40",
            "type": "Repair Services",
            "image": "/images/book/washing-machine.jpg"
        }
    ],
    "code": "200"
}
```

#### Category

`GET Request`

```
Endpoint  /api/v1/services/category/[{term}]
```

> Noteworthy: `[{term}]` accepts `String` type of service eg: Photography or Repair Services ...etc

Sample Response

```
{
    "status": "success",
    "response": [
        {
            "id": "1039",
            "title": "Baby Photoshoot",
            "keywords": "Baby Shower, Infant, Child, Photographer, Photography",
            "worker": "Baby Photographers",
            "price": "GHS 305 to GHS 800",
            "type": "Photography",
            "image": "/images/book/baby.jpg"
        },
        {
            "id": "1040",
            "title": "Pre Wedding Photoshoot",
            "keywords": "Weddings, Photographers",
            "worker": "Pre Wedding Photographers",
            "price": "GHS 1,232 to GHS 2,100",
            "type": "Photography",
            "image": "/images/book/pre-wedding.jpg"
        }
    ],
    "code": "200"
}
```

#### Popular Services

`GET Request`

```
Endpoint  /api/v1/services/popular
```

Sample Response

```
{
    "status": "success",
    "response": [
        {
            "id": "1013",
            "title": "House Cleaning",
            "keywords": "Kitchen Cleaning, Bathroom Cleaning, Home Cleaning, Deep Home Cleaning",
            "worker": "Home Cleaners",
            "price": "GHS 30 to GHS 462",
            "type": "Home Improvements",
            "image": "/images/book/clean.jpg"
        },
        {
            "id": "1037",
            "title": "Events and Wedding Planning",
            "keywords": "Wedding Planners, Party Planners",
            "worker": "Event Planners",
            "price": "GHS 1,800 to GHS 3,000",
            "type": "Events & Weddings",
            "image": "/images/book/event.jpg"
        }
    ],
    "code": "200"
}
```